/*
 * Copyright (c) 2016, 2018 Gluon
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of Gluon, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GLUON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.gluonhq.samples.connect.rest;

import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.connect.GluonObservableList;
import com.gluonhq.connect.converter.InputStreamIterableInputConverter;
import com.gluonhq.connect.provider.DataProvider;
import com.gluonhq.connect.provider.RestClient;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

public class RestListView extends View {

    public RestListView() {

       // String raw = "https://www.googleapis.com/books/v1/volumes?q=%s&maxResults=%d&startIndex=%d";
       //String formatted = String.format(raw, search, maxResults, startIndex);



        // create a RestClient to the specific URL
        RestClient restClient = RestClient.create()
                .method("GET")
                .host("https://www.googleapis.com/books")
                .path(String.format("/v1/volumes?q=%s&maxResults=%d&startIndex=%d", "java", 40, 0));

        // create a custom Converter that is able to parse the response into a list of objects
        InputStreamIterableInputConverter<VolumesResponse> converter = new ItemsIterableInputConverter<>(VolumesResponse.class);

        // retrieve a list from the DataProvider
//        GluonObservableList<Item> books =
//                DataProvider.retrieveList(restClient.createListDataReader(converter));


//        System.out.println(books);
//        // create a JavaFX ListView and populate it with the retrieved list
//        ListView<Item> lvErrors = new ListView<>(books);
//
//        lvErrors.setCellFactory(lv -> new ListCell<Item>() {
//            @Override
//            protected void updateItem(Item item, boolean empty) {
//                super.updateItem(item, empty);
//
//                if (!empty) {
//                    setText(item.getId() + " - " + item.getPublisher());
//                } else {
//                    setText(null);
//                }
//            }
//        });
//
//        setCenter(lvErrors);
    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> getApplication().getDrawer().open()));
        appBar.setTitleText("Rest List Viewer");
    }
}
